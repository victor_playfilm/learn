from asciimatics.screen import Screen
from greetings import demo


TRUE_OR_FALSE_DICT = dict()

TRUE_OR_FALSE_DICT['key_1'] = 'true'
TRUE_OR_FALSE_DICT['key_2'] = 'false'
TRUE_OR_FALSE_DICT['key_3'] = True
TRUE_OR_FALSE_DICT['key_4'] = False
TRUE_OR_FALSE_DICT['key_5'] = 1
TRUE_OR_FALSE_DICT['key_6'] = 0
TRUE_OR_FALSE_DICT['key_7'] = 'Mustang'

print(f"Dict for test {TRUE_OR_FALSE_DICT}")
input("------------------------- ENTER TO CONTINUE -------------------------")
for key in TRUE_OR_FALSE_DICT.keys():
    print(f"KEY : {key} VALUE: {TRUE_OR_FALSE_DICT[key]}")
    print(f"IS EQUALS TO TRUE? {TRUE_OR_FALSE_DICT.get(key) == True}")
    print(f"IS EQUALS TO FALSE? {TRUE_OR_FALSE_DICT.get(key) == False}")
    print(f"IS EQUALS TO 0? {TRUE_OR_FALSE_DICT.get(key) == 0}")
    print(f"IS EQUALS TO 1? {TRUE_OR_FALSE_DICT.get(key) == 1}")
    input("------------------------- ENTER TO CONTINUE -------------------------")

print(f"WHAT HAPPEN IF THE KEYS DOESN'T EXISTS?")
try:
    print (TRUE_OR_FALSE_DICT['key_8'])
except Exception as error:
    print ("KeyError: 'key_8'")
input("------------------------- ENTER TO CONTINUE -------------------------")

print(f"WE CAN FIX THIS USING A DEFAULT")
default_values = ['true', 'false', True, False, 1, 0, 'Stallion']
for default_value in default_values:
    print(f"DEFAULT VALUE : {default_value}")
    print(f"IS EQUALS TO TRUE? {TRUE_OR_FALSE_DICT.get('key_8', default_value) == True}")
    print(f"IS EQUALS TO FALSE? {TRUE_OR_FALSE_DICT.get('key_8', default_value) == False}")
    print(f"IS EQUALS TO 0? {TRUE_OR_FALSE_DICT.get('key_8', default_value) == 0}")
    print(f"IS EQUALS TO 1? {TRUE_OR_FALSE_DICT.get('key_8', default_value) == 1}")
    input("------------------------- ENTER TO CONTINUE -------------------------")

input("------------------------- LEARN COMPLETE CTRL - C FOR EXIT AFTER GREETINGS-------------------------")
Screen.wrapper(demo)









